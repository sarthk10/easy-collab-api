# project-managment-tool-api

### Development

✅ `git clone clone-path`

✅ Run `yarn install` to install all the dependancies.

✅ Run `yarn dev` to run production server.

✅ Run `yarn lint` to check linter's errors.

✅ Run `yarn test` to run testing server.

### make sure you change `.env` with appropriate details


## Production Guidelines

- Exclude fsevents by setting - ```yarn config set ignore-engines true```

- Installing packages - ```yarn install --production```




